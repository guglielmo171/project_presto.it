<?php

namespace App\Http\Livewire;

use App\Models\Image;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateAnnouncements extends Component
{   
    use WithFileUploads;
    
    public $title;
    public $price;
    public $body;
    public $category_id=1;
    public $message;
    public $validated;
    public $temporary_images;
    public $images = [];
    public $announcement;

    protected $rules=[
        'title'=>'required',    
        'price'=>'numeric',       
        'body'=>'required',
        'category_id'=>'required',
        'images.*'=> 'image|max:2048',
        'temporary_images.*'=> 'image|max:2048'       

    ];

    protected $messages=[
        'title.required'=>'Inserisci il titolo',
        'price.required'=>'Inserisci un prezzo',
        'price.numeric'=>'Il prezzo deve essere un numero',
        'body.required'=>'Inserisci una descrizione',
        'category_id.required'=>'Seleziona almeno una categoria',
        'temporary_images.required'=>'L\'immagine è richiesta',
        'temporary_images.*.image'=>'I file devono essere immagini',
        'temporary_images.*max'=>'L\'immagine dev\'essere massimo 2mb',
        'images.image'=> 'L\'immagine dev\'essere un\'immagine',
        'images.max'=> 'L\'immagine dev\'essere massimo di 2mb'
    ];

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function updatedTemporaryImages(){
        if ($this->validate([
            'temporary_images.*'=>'image|max:1024',
        ])) {
            foreach ($this->temporary_images as $image) {
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key){
        if (in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }


    public function createAnnouncements(){

        $this->validate();

        $category = Category::find($this->category_id);

        $this->announcement = Category::find($this->category_id)->announcements()->create($this->validate());

        if(count($this->images)){

            foreach($this->images as $image) {
                
                $newFileName = "announcement/{$this->announcement->id}";
                $newImage = $this->announcement->images()->create([
                    'path'=>$image->store($newFileName, 'public')
                ]);

                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 397.6, 432),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),                   
                ])->dispatch($newImage->id);
                 
            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }

        $this->reset();

        return redirect(route('welcome'))->with('message','Annuncio creato , attendi la conferma del Revisore!');
    }


    public function render()
    {   
        $categories=Category::all();
        return view('livewire.create-announcements', compact('categories'));
    }
}
