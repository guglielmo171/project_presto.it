<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{

    public function __construct()
    {
        // $this->middleware('verified');
    }
    public function welcome() {
        $announcements=Announcement::all()->sortByDesc('created_at')->take(3);
        $categories=Category::all();
        return view('welcome', compact('announcements', 'categories'));
        
    }

    public function setLanguage($lang){
        // dd($lang);
        session()->put('locale', $lang);
        return redirect()->back();
    }

    // public function publicIndex(Category $category){
    //     $announcements=Announcement::where('category', $category)->get();
    // }
}


