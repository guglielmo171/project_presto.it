<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('welcome','set_language_locale');
    }

    public function profile(){
        return view('user.profile');
    }
}
