<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\BecomeRevisor;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index()
    {
        $announcement_to_check = Announcement::where('is_accepted', null)->first();
        $goBack = Announcement::orderBy('updated_at', 'DESC')->whereIn('is_accepted', [true, false])->first();
        if($goBack){
            return view('revisor.index', compact('announcement_to_check', 'goBack'));
        } else{
            $goBack=Announcement::orderBy('updated_at', 'DESC')->first();
            return view('revisor.index', compact('announcement_to_check', 'goBack'));
        }
    }
    
    public function goBack(Announcement $goBack){
        if (($goBack->is_accepted === 1)||($goBack->is_accepted === 0)){
            $goBack->setAccepted(null);
            return redirect()->back()->with('message','Hai annullato l\'ultima modifica');
        }
        else{
            return redirect()->back()->with('message','Non ci sono azioni fa modificare');
        } 
    }

    public function acceptAnnouncement(Announcement $announcement)
    {
        $announcement->setAccepted(true);
        return redirect()->back()->with('message'
        , 'Complimenti, è stato accettato l\'annuncio');
    }

    public function rejectAnnouncement(Announcement $announcement)
    {
        $announcement->setAccepted(false);
        return redirect()->back()->with('message'
        , 'Caspita, è stato rifiutato l\'annuncio');
    }

    public function becomeRevisor(){
        Mail::to('user@mail.com')->send(new BecomeRevisor(Auth::user()));
        return redirect()->back()->with('message', 'Richiesta inviata con successo!');
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:makeUserRevisor', ['email'=>$user->email]);
        return redirect('/')->with('message', 'Sei stato approvato come revisore');
    }
}
