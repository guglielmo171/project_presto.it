<?php

use App\Http\Controllers\AnnouncementController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'welcome'])->name('welcome');

Route::get('/announcement-create', [AnnouncementController::class, 'create'])->middleware('auth')->name('create.announcement');
Route::get('/announcement-index', [AnnouncementController::class, 'index'])->name('index.announcement');
Route::get('/announcement-show/{announcement}', [AnnouncementController::class, 'show'])->name('index.show');

Route::get('/categories-show/{category}', [AnnouncementController::class, 'categoryShow'])->name('categoryShow');

// Rotte revisore 

Route::get('/revisor/home', [RevisorController::class, 'index'])->middleware('isRevisor')->name('revisor.index');

// Accetta annuncio 
Route::patch('/accetta/annuncio/{announcement}', [RevisorController::class, 'acceptAnnouncement'])->middleware('isRevisor')->name('revisor.accept_announcement');

// Rifiuta annuncio 
Route::patch('/rifiuta/annuncio/{announcement}', [RevisorController::class, 'rejectAnnouncement'])->middleware('isRevisor')->name('revisor.reject_announcement');

// Diventare un revisore
Route::get('/become/revisor', [RevisorController::class, 'becomeRevisor'])->middleware('auth')->name('become.revisor');

// Rendi un utente revisore
Route::get('/rendi/revisore/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');

// GoBACKpls
Route::patch('/annulla/{goBack}',[RevisorController::class, 'goBack'])->middleware('isRevisor')->name('revisor.goBack');
// cerca annuncio
Route::get('/ricerca/annuncio', [AnnouncementController::class, 'searchAnnouncements'])->name('announcements.search');

Route::get('/profile', [UserController::class, 'profile'])->name('profile');

// Cambia lingua
Route::post('/lingua/{lang}', [PublicController::class, 'setLanguage'])->name('set_language_locale');

// Bottone back
Route::get('/back', [AnnouncementController::class, 'back'])->name('back');