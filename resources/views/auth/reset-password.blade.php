<x-layout>
    @vite(['resources/css/profile.css', 'resources/js/profile.js'])

    <div class="container-fluid d-flex justify-content-center mt-5" id="logincontainer">
        <div class="login-card me-5" style="place-items: center; height: 60%;">
            <h3 style="font-size: 3rem!important;" class=" text-center text-uppercase">{{__('ui.forgotpassword')}}?</h3>
    @if (session('status'))
        <div data-bs-auto-close="true" class="alert alert-success alert-dismissible fixed-top">
            <p>{{session('status')}}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>       
    @endif
    <form method="POST" action="/reset-password">
        @csrf
        <input type="hidden" name="token" value="{{request()->route('token')}}">
        <div class="form-outline mb-4">
            <label class="form-label" for="form1Example1">{{__('ui.email')}}</label>
            <input type="email" name="email" id="form1Example1" class="@error('email') is-invalid @enderror control" value="{{$request->email}}" />
            @error('email')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-outline mb-4 password">
            <label class="form-label" for="password">Password</label>
            <input placeholder="Password" type="password" name="password" id="password" class="@error('password') is-invalid @enderror control" />
            @error('password')
                <span class="text-danger">{{ $message }}</span>
            @enderror
          </div>
            <!-- Password_confirmation input -->
          <div class="form-outline mb-4 password">
             <label class="form-label" for="password_confirmation">{{__('ui.confirm')}} Password</label>
             <input placeholder="Conferma Password" type="password" name="password_confirmation" id="password_confirmation password" class="@error('password_confirmation') is-invalid @enderror control" />
            @error('password_confirmation')
                <span class="text-danger">{{ $message }}</span>
            @enderror
          </div>
        <button style="    margin-right: 2rem;" type="submit" class="btn btn-pr control">{{__('ui.change')}} PASSWORD</button>
        </div>
    </form>
</div>

</x-layout>