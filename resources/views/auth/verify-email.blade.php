<x-layout>
    <h2 class="display-6 text-uppercase text-center">{{__('ui.thankYou')}}</h2>
   
    <div class="container-fluid p-5 negative-margin-top">
        
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 text-center">
              <p class="useritem">{{__('ui.waitASec')}}</p>
                @if (session('status') == 'verification-link-sent')
                    <div data-bs-auto-close="true" class="alert alert-success fade show alert-dismissible fixed-top">
                        {{__('ui.confrimMail')}}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <form action="{{route('verification.send')}}" method="post">
                    @csrf
                    <button class="btn btn-block btn-pr" type="submit">{{__('ui.confirmMailAgain')}}</button>
                </form>
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <p class="useritem">{{__('ui.gotAProblem')}}</p>
                    <button class="btn btn-block btn-pr" type="submit">Logout</button>
                </form>
            </div>
        </div>
    </div>


</x-layout>