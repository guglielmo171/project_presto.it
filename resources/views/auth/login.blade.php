<x-layout>
    <main>


        @vite(['resources/css/login.css', 'resources/js/login.js'])
        <div id="logincontainer" class="container">
            <form method="POST" action="{{route('login')}}">
                @csrf
                <div class="login-card">
    
                    <h2>{{__('ui.login')}}</h2>
                    
                    <h3>{{__('ui.credentialsinput')}}</h3>
                    
                    <div class="login-form">
                        <!-- Email input -->
                        <div class="form-outline mb-4">
                            <label class="form-label" for="form1Example1">{{__('ui.email')}}</label>
                        <input type="email" name="email" id="form1Example1" class="@error('email') is-invalid @enderror control" placeholder="Email" />
                        @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                    
                        <!-- Password input -->
                        <div class="form-outline mb-4">
                            <label class="form-label" for="password">Password</label>
                        <input placeholder="Password" type="password" name="password" id="password" class="@error('password') is-invalid @enderror control"/>
                        @error('password')
                                    <span class="text-danger">{{ $message }}</span>
                        @enderror
                        </div>
                        <!-- Submit button -->
                        <button style="    margin-right: 2rem;" type="submit" class="btn btn-pr control">{{__('ui.login')}}</button>
                        <a href="{{route('register')}}">{{__('ui.noregistered')}}</a>
                        <a class="small" href="/forgot-password">{{__('ui.forgotpassword')}}</a>

                    
                    </div>
                </div>  
            </form>
        </div>
    </main>
</x-layout>