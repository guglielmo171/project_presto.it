<x-layout>
  <main>



    @vite(['resources/css/register.css', 'resources/js/register.js'])
    <div class="container" id="logincontainer">

        <form method="POST" action="{{route('register')}}">
            @csrf
          <div class="login-card">
            <h2>{{__('ui.register')}}</h2>
            <h3>{{__('ui.credentialsinput')}}</h3>
            <div class="login-form">
                {{-- name input --}}
              <div class="form-outline mb-4">
                <label class="form-label" for="name">{{__('ui.name')}}</label>
                <input type="text" name="name" id="name" class="@error('name') is-invalid @enderror control" placeholder="{{__('ui.name')}}"/>
                @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                @enderror
              </div>
                <!-- Email input -->
              <div class="form-outline mb-4">
                <label class="form-label" for="form1Example1">{{__('ui.email')}}</label>
                <input type="email" name="email" id="form1Example1" class="@error('email') is-invalid @enderror control" placeholder="{{__('ui.email')}}" />
                @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                @enderror
              </div>
                <!-- Password input -->
              <div class="form-outline mb-4 password">
                <label class="form-label" for="password">Password</label>
                <input placeholder="Password" type="password" name="password" id="password" class="@error('password') is-invalid @enderror control" />
                @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                @enderror
              </div>
                <!-- Password_confirmation input -->
              <div class="form-outline mb-4 password">
                 <label class="form-label" for="password_confirmation">{{__('ui.confirm')}} Password</label>
                 <input placeholder="{{__('ui.confirm')}} Password" type="password" name="password_confirmation" id="password_confirmation password" class="@error('password_confirmation') is-invalid @enderror control" />
                 @error('password_confirmation')
                                            <span class="text-danger">{{ $message }}</span>
                @enderror
              </div>
            </div>
            
          
            <!-- Submit button -->
            <button style="    margin-right: 2rem;" type="submit" class="btn btn-pr control">{{__('ui.register')}}</button>
            <a href="{{route('login')}}">{{__('ui.alreadyregistered')}}</a>
          </div>
        </form>
    </div>  
  </main>
</x-layout>
