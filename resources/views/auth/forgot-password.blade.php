<x-layout>
    @vite(['resources/css/profile.css', 'resources/js/profile.js'])
    <main>

        <div class="container-fluid d-flex justify-content-center mt-5" id="logincontainer">
            
            <div class="login-card">

                @if (session('status'))
                    <div data-bs-auto-close="true" class="alert alert-success alert-dismissible fixed-top">
                        <p>{{session('status')}}</p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>       
                @endif
            <form method="POST" action="/forgot-password">
                @csrf
                <div class="form-outline mb-4">
        <h3 class="text-center">{{__('ui.forgotPassword')}}</h3>

                    <label class="form-label" for="form1Example1">{{__('ui.email')}}</label>
                    <input type="email" name="email" id="form1Example1" class="@error('email') is-invalid @enderror control"  />
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <button style="    margin-right: 2rem;" type="submit" class="btn btn-pr control mt-5">{{__('ui.newPassword')}}</button>
                <a  class="btn btn-pr control pt-3 mt-5" href="{{route('login')}}">{{__('ui.backToLogin')}}</a>
            </div>

                
            </form>
        </div>
    </main>
</x-layout>