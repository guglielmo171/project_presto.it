<x-layout>
    @vite(['resources/css/profile.css', 'resources/js/profile.js'])

    <main>

        <div class="container carta mt-5" id="logincontainer">


            @if (session('status') === 'profile-information-updated')
                <div class="alert alert-success alert-dismissible fade show mb-4 fixed-top">
                   <p>{{__('ui.profile')}}</p>
                   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> 
                </div>
            @endif
            @if (session('status') === 'password-updated')
                <div class="alert alert-success alert-dismissible fade show mb-4 fixed-top">
                   <p>{{__('ui.pwdupt')}}</p> 
                   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="login-card me-5 " style="place-items: center; color: #003452;;">
                <div class="container-fluid display-5">
                    <div class="row">
                        <div class="col-12  display-5
                        ">
                            <h2>{{__('ui.profileof')}} {{Auth::user()->name}}</h2>

                            <p>{{__('ui.name')}}: {{Auth::user()->name}}</p>
                            <p>{{__('ui.email')}}: {{Auth::user()->email}}</p>
                    
                            <p>{{__('ui.verified')}}: <i class="{{Auth::user()->email_verified_at ? 'fa-sharp fa-solid fa-circle-check text-success' : 'fa-solid fa-circle-xmark text-danger'}}"></i>  </p>
                            @if (Auth::user()->email_verified_at === null)
                                        <span id="dimensione" class="text-danger ">Per verificare il tuo Profilo , clicca il link che hai ricevuto via mail.</span>
                            @endif
                            <p>{{__('ui.profilecrtat')}}: {{Auth::user()->created_at->format('d/m/Y')}}</p>
                            <p>{{__('ui.profileuptat')}}: {{Auth::user()->updated_at->format('d/m/Y')}}</p>
                        </div>
                        <div class="col-12 ">
                            <form action="/user/profile-information" method="post">
                                @csrf
                                @method('put')

                                <div class="login-form ">
                                    <div class="form-outline mb-4">
                                        <h3>{{__('ui.uptdataprofile')}}</h3>

                                        <label class="form-label" for="name">{{__('ui.name')}}</label>
                                        <input type="text" name="name" id="name" class=" control @error('name') is-invalid @enderror" placeholder="Nome"/>
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        
                                    </div>
                                        <!-- Email input -->
                                    <div class="form-outline mb-4">
                                        <label class="form-label" for="form1Example1">{{__('ui.emailaddress')}}</label>
                                        <input type="email" name="email" id="form1Example1" class="@error('email') is-invalid @enderror control" placeholder="Email" />
                                        @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <button style=" " type="submit" class="w-50 btn btn-pr control">{{__('ui.dataupt')}}</button>
                            </form>
                        </div>
                        <div class="col-12 ">
                            <form style="margin-bottom: 159px;" action="/user/password" method="post">
                                @csrf
                                @method('put')
                                <div class="form-outline mb-4" >
                                    <h3>{{__('ui.pwdupt1')}}</h3>
            
                                    <label class="form-label" for="name">Password {{__('ui.current')}}</label>
                                    <input type="password" name="current_password" id="current_password" class=" control @error('current_password') is-invalid @enderror"/>
                                    @error('current_password')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="form1Example1">{{__('ui.new')}} Password</label>
                                    <input type="password" name="password" id="password" class="@error('password') is-invalid @enderror control"  />
                                    @error('password')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-outline mb-4 password">
                                    <label class="form-label" for="password_confirmation">{{__('ui.confirm')}} Password</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class=" control @error('password_confirmation') is-invalid @enderror" />
                                    @error('password_confirmation')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                 </div>
                                <button style="" type="submit" class="w-50 btn btn-pr control">{{__('ui.update')}} PASSWORD</button>
                            
                            </form>
                        </div>
                    </div>
                </div>
                
                

            </div>


                 {{-- 
            <div class="login-card card" style="padding-bottom: 30rem">

               
            </div> --}}
    </main>

</x-layout>