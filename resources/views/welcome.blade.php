<x-layout>

<main class="two">
    <h1 id="presto" class="display-1 text-center mt-5">PRESTO.IT</h1>
    <p class=" text-center">
        <a href="#hr"><i style="font-size: 4rem; color:var(--sapphire-blue);" class="text-center fa-solid fa-angles-down fa-bounce fa-2x"></i>
            </a>

        </p>

<div class="carta">
    <div class="container-fluid logohome">
        <img class="logohome1" src="/media/logo.gif" alt="">
    </div>

</div>
     <h3 id="mainheader" class=" display-3 mt-5 text-center">
         {{__('ui.solid')}} •  {{__('ui.safe')}}  •  {{__('ui.usable')}}
     @if (session('message'))
         <div data-bs-auto-close="true" class="alert fade show alert-success alert-dismissible fixed-top">
             {{ session('message') }}
             <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         </div>
     @endif
 
     @if (session('access.denied'))
         <div class="alert alert-danger fade show fixed-top">
             {{ session('access.denied') }}
             <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         </div>
     @endif
     <h5 class="text-center display-5 last">{{__('ui.allAnnouncement')}}</h5>
     <hr id="hr">
    
    <div class="container">

        <div class="row mb-5">

            @forelse($announcements as $announcement)
           
                @if($announcement->is_accepted == true)

                    <div class="col-12 col-xxl-4">

                        <div class="card" style="place-items: center">

                            <h5 class="card-title">{{__('ui.title')}}:{{$announcement->title}}</h5>

                            <img class="w-100  h-75" src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(397.6, 432) : "https://picsum.photos/200/300"}}" alt="image">           

                            <p class="card-text">{{__('ui.catNav')}}: {{$announcement->category->name}}</p>

                            <p class="card-text">{{__('ui.price')}}: {{$announcement->price}}$</p>

                            <p class="card-text">{{__('ui.description')}}: {{$announcement->body}}</p>

                            <a href="{{route('index.show', compact('announcement'))}}" class="btn btn-primary mt-2 w-100">{{__('ui.details')}}</a>

                        </div>
                    </div>

                @endif

                @empty

                    <h1 class="text-center mt-5 display-1 mb-5">{{__('ui.noAnnunci')}}</h1>

            @endforelse

            </div>

        </div>

    </div>
    

     
</main>
<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

</x-layout>