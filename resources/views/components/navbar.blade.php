<nav class="navbar navbar-expand-lg bg-light one">
  <div class="container-fluid ">
    <a href="{{route('welcome')}}">
      <img class="navbar-brand logo" src="/media/logo.gif" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fa-sharp fa-solid fa-bars-staggered"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav fix  me-auto mb-2 mb-lg-0 ">
        <li class="nav-item ">
          <a class="nav-link {{Route::is('welcome') ? 'active' : ''}} " aria-current="page" href="{{route('welcome')}}">Home</a>
        </li>
        @auth
            <li><a class="dropdown-item {{Route::is('create.announcement') ? 'active' : ''}} nav-link" href="{{route('create.announcement')}}">
              {{__('ui.addAnnouncement')}}</a></li>
        @endauth
        <li><a class="nav-link {{Route::is('index.announcement') ? 'active' : ''}}" href="{{route('index.announcement')}}">{{__('ui.allAnnouncementNav')}}</a></li>
        <li class="nav-item dropdown ">
          <a class="nav-link dropdown-toggle " data-bs-auto-close="outside" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            {{__('ui.catNav')}}
          </a>
          <ul class="dropdown-menu bg-light">
            
            <li><hr class="dropdown-divider"></li>
            <li class="dropend">
              @foreach($categories as $category)
                <li><hr class="dropdown-divider"></li>
                          @switch($category->name)
                            @case('Abbigliamento')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Clothing')}}</a></li>
                            @break
                            @case('Arredamento')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Furnitures')}}</a></li>
                            @break
                            @case('Sport')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Sport')}}</a></li>
                            @break
                            @case('Libri')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Libri')}}</a></li>
                            @break
                            @case('Fumetti')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Fumetti')}}</a></li>
                            @break
                            @case('Antiquariato')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Antiquariato')}}</a></li>
                            @break
                            @case('Giardinaggio')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Giardinaggio')}}</a></li>
                            @break
                            @case('Motori')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Motori')}}</a></li>
                            @break
                            @case('Tecnologia')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Tecnologia')}}</a></li>
                            @break
                            @case('Cucina')
                                <li><a class="dropdown-item nav-link" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Cucina')}}</a></li>
                            @break
                          @endswitch
                @endforeach
                <li><a class="dropdown-item nav-link" href="{{route('index.announcement')}}">{{__('ui.allAnnouncementNav')}}</a></li>

            </li>
          </ul>
          
        </li>

     
        
        @if (Auth::user() && Auth::user()->is_revisor)
              <li class="nav-item">
                <a href="{{route('revisor.index')}}" class="nav-link btn btn-sm position-relative {{Route::is('revisor.index') ? 'active' : ''}}" aria-current="page">
                  {{__('ui.areaRevisor')}}
                  <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                    {{App\Models\Announcement::toBeRevisionedCount()}}
                    <span class="visually-hidden">{{__('ui.unreadMessages')}}</span>
                  </span>
                </a>
              </li>
        @endif
        <li id="ricerca1">

          <form class="d-flex" action="{{route('announcements.search')}}" method="GET">
            <div class="box">
              <div class="container-2">
                  <input id="search"  class="form-control me-2" type="search" name="searched" aria-label="Search">
              </div>
            </div>
            <button id="ricerca" class="icon btn " type="submit">                  <span class="icon"><i class="fa fa-search"></i></span>
            </button>
          </form>
        
        </li>
      </ul>
   
      <li class="nav-item dropdown me-2 " style="list-style: none">
        <a
          class="nav-link dropdown-toggle"
          href="#"
          id="navbarDropdown"
          role="button"
          data-bs-toggle="dropdown"
          aria-expanded="true"
        >
        Lingue
        </a>
        <ul class="dropdown-menu" id="lang">
          <li>
            <x-_locale lang="it"/>
          </li>
          <li>
            <x-_locale lang="en" />
          </li>
          <li>
            <x-_locale lang="es" />
          </li>
        </ul>
      </li>
      @guest
          <a class="usermessage nav-link checkuser"  href="{{route('login')}}">
           {{__('ui.accessHi')}} 
          </a>
        @else
          <p class="usermessage nav-link checkuser">{{__('ui.welcomeAuth')}} {{Auth::user()->name}}</p>
        @endguest
      <div id="avatar" class="dropdown positionicon">
        <a
          class="dropdown-toggle d-flex align-items-center hidden-arrow"
          href="#"
          id="navbarDropdownMenuAvatar"
          role="button"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          <img
            src="https://w7.pngwing.com/pngs/223/244/png-transparent-computer-icons-avatar-user-profile-avatar-heroes-rectangle-black.png"
            class="rounded-circle"
            height="25"
            alt="Black and White Portrait of a Man"
            loading="lazy"
          />
        </a>
        <ul id="authdropdown"
          class="dropdown-menu dropdown-menu-end"
          aria-labelledby="navbarDropdownMenuAvatar" id="avatardropdown"
        >
        @guest
          <li>
            <a  class="useritem dropdown-item" href="{{route('login')}}">{{__('ui.login')}}</a>
          </li>
          <li>
            <a class="useritem dropdown-item" href="{{route('register')}}">{{__('ui.register')}}</a></a>
          </li>
        @else  
        <li>
          <a class="useritem dropdown-item" href="{{route('profile')}}">{{__('ui.profiledropdown')}}</a>
        </li>
          <li>
            <a class="useritem dropdown-item" href="" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">{{__('ui.logout')}}</a>
            <form  class="d-none" action="{{route('logout')}}" method="post" id="form-logout">
              @csrf
            </form>
          </li>
        @endguest
        </ul>
      </div>
      <i id="dark-mode-toggle" class="fa-solid fa-sun positionicon"></i>

    </div>
    <!-- Right elements -->
  </div>
      

    </div>
  </div>
</nav>



