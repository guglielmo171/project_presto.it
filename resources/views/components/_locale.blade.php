<form action="{{route('set_language_locale', $lang)}}" class="d-inline" method="post">

    @csrf

    <button type="submit" class="btn w-100">
        <img src="{{ asset('vendor/blade-flags/language-'. $lang .'.svg')}}" width="16" height="16" />
    </button>

</form>