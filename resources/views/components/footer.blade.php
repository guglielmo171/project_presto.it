    <!-- Footer -->
    <footer class="bg-light  text-center text-white absolute three fixed-bottom">
      <!-- Grid container -->
      <div class="container p-4 pb-0">
        <!-- Section: CTA -->
        
        <!-- Section: CTA -->
      </div>
      <!-- Grid container -->
      {{-- <i  class="fas fa-adjust"></i> --}}

      <!-- Copyright -->
      <div class="text-center p-3">
        © 2022 Copyright:
        <a class="text-white" href="{{route('welcome')}}">Presto.it</a>

        @if(Auth::user() && Auth::user()->is_revisor)
            
            <span class="border-bottom border-white rounded p-2 mx-2">{{__('ui.hiRevisor')}}</span>

        @else

            <a href="{{route('become.revisor')}}"  class="btn btn-outline-light btn-rounded mx-2">
            {{__('ui.becomeRevisor')}}
          </a>

        @endif
      </div>
      <!-- Copyright -->
    </footer>
    <!-- Footer -->
