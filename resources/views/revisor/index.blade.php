<x-layout>
  <main>





      <div class="container">
        <h2>{{__('ui.Revisione')}}</h2>
        <div class="row">
            <div class="col-12">
              @if($announcement_to_check)
              <h2>
                {{__('ui.AnnuncioDaRevisionare')}}
              </h2>
              @else
              {{__('ui.NoAnnunciRevisione')}}
                <h2>
              @endif
            </div>
        </div>
      </div>
    
    
    
    
  @if($announcement_to_check)

    <div class="container-fluid">
          <div class="row">
            <div class="col-6">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">    

                @if ($announcement_to_check->images)

                    <div class="carousel-inner"> 

                      @foreach($announcement_to_check->images as $image)

                        <div class="carousel-item @if($loop->first)active @endif">
                          <img src="{{!$announcement_to_check->images()->get()->isEmpty() ? $announcement_to_check->images()->first()->getUrl(397.6, 432) : "https://picsum.photos/200/300"}}" class="img-fluid p-3 rounded" alt="ImmagineArticolo">
                        </div>

                      @endforeach

                    </div>

                @else 

                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img src="https://picsum.photos/id/27/1200/400?1" class="img-fluid p-3" alt="">
                        </div>
                        <div class="carousel-item active">
                          <img src="https://picsum.photos/id/27/1200/400" class="img-fluid p-3" alt="">
                        </div>
                        <div class="carousel-item active">
                          <img src="https://picsum.photos/id/27/1200/400" class="img-fluid p-3" alt="">
                        </div>
                      </div>

                @endif

                  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                    
                </div>  
                
              
            </div>
            <div class="col-6">
              <h5 class="card-title">{{__('ui.title')}}: {{$announcement_to_check->title}}</h5>
              <p class="card-text">{{__('ui.catNav')}}: {{$announcement_to_check->category->name}}</p>
              
              <p class="card-text">{{__('ui.description')}}: {{$announcement_to_check->body}}</p>
              <p class="card-text">{{__('ui.CreatoIl')}}: {{$announcement_to_check->created_at->format('d/m/Y')}}</p>
              <p class="card-text">{{__('ui.price')}}: {{$announcement_to_check->price}} €</p>
              <hr>
              <h5>Tag</h5>
              @if ($image->labels)
                  @foreach ($image->labels as $label)
                      <p class="d-inline">{{$label}}---</p>
                  @endforeach
              @endif
              <hr>
              <h5>{{__('ui.ControlloImmagini')}}</h5>
              <p class="card-text">{{__('ui.Adulti')}}: <span class="{{$image->adult}}"></span></p>
              <p class="card-text">{{__('ui.Satira')}}: <span class="{{$image->spoof}}"></span> </p>
              <p class="card-text">{{__('ui.Medicina')}}: <span class="{{$image->medical}}"></span> </p>
              <p class="card-text">{{__('ui.Violenza')}}: <span class="{{$image->violence}}"></span> </p>
              <p class="card-text">{{__('ui.ContenutoAmmiccante')}}: <span class="{{$image->racy}}"></span> </p>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-4">
                <form action="{{route('revisor.accept_announcement',['announcement'=>$announcement_to_check])}}" method="POST">
              @csrf
              @method('PATCH')
              <button type="submit" class="btn btn-success shadow">{{__('ui.accetta')}}</button>
                </form>
            </div>
            <div class="col-12 col-md-4">
                <form action="{{route('revisor.reject_announcement',['announcement'=>$announcement_to_check])}}" method="POST">
              @csrf
              @method('PATCH')
              <button type="submit" class="btn btn-success shadow">{{__('ui.rifiuta')}}</button>
                </form>
          </div>
  @endif
            <div class="col-12 col-md-4">
              <form action="{{route('revisor.goBack', ['goBack'=>$goBack])}}" method="POST" >
                @csrf
                @method('PATCH')
                <button type="submit" class="btn btn-danger ">{{__('ui.goback')}}</button>
              </form>
            </div>
            
    </div>
    <div class="row mt-3">
      
    </div>
    </div>
  </main>
</x-layout>