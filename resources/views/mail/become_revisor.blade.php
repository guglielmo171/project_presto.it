<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=ù, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <h2>{{__('ui.becomerevisortitle')}}</h2>
        <p>{{__('ui.info')}}:</p>
        <p>{{__('ui.name')}}: {{$user->name}}</p>
        <p>{{__('ui.email')}}: {{$user->email}}</p>
        <p>{{__('ui.becomerevisorp')}}:</p>
        <a href="{{route('make.revisor', compact('user'))}}">{{__('ui.makerevisor')}}</a>
    </div>

</body>
</html>
    
