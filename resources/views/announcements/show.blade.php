<x-layout>
  
    {{-- carosello --}}
    <div class="swiper mySwiper">
      @if ($announcement->images)

        <div class="swiper-wrapper">

          @foreach($announcement->images as $image)

            <div class="swiper-slide">

                <img  src="{{Storage::url($image->path)}}" alt="image">

                <button id="descrizione" class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasScrolling">{{__('ui.details')}}</button>

            </div>
          @endforeach

        </div>

      @else

        <div class="swiper-wrapper">
          <div class="swiper-slide">
              <img  src="https://picsum.photos/600/300" alt="image">
              <button id="descrizione" class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasScrolling">{{__('ui.details')}}</button>
          </div>
          <div class="swiper-slide"><img src="https://picsum.photos/600/300" alt="image">
            <button id="descrizione" class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasScrolling">{{__('ui.details')}}</button>
          </div>
          <div class="swiper-slide"><img src="https://picsum.photos/600/300" alt="image">
            <button id="descrizione" class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasScrolling">{{__('ui.details')}}</button>
          </div>
          <div class="swiper-slide"><img src="https://picsum.photos/600/300" alt="image">
            <button id="descrizione" class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasScrolling">{{__('ui.details')}}</button>
          </div>
        </div>
      @endif
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-pagination"></div>
    </div>
    
      <div class="offcanvas offcanvas-end" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
        <div class="offcanvas-header">
          <h5 class="offcanvas-title" id="offcanvasRightLabel">{{__('ui.details')}}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
          <h5 class="card-title">{{__('ui.title')}}: {{$announcement->title}}</h5>
                <p class="card-text">{{__('ui.catNav')}}: {{$announcement->category->name}}</p>
                <hr>
                <p class="card-text">{{__('ui.price')}}: {{$announcement->price}}$</p>
                <hr>
                
                <p class="card-text">{{__('ui.description')}}: {{$announcement->body}}</p>
                <hr>
  
                <p class="card-text">{{__('ui.createdAt')}}: {{$announcement->user->name}}</p>
                <hr>
  
                <p class="card-text">{{__('ui.created')}}: {{$announcement->created_at}}</p>
                <hr>
                        
                  <a href="{{url()->previous()}}" class="btn-back">
                   <button class="btn btn-back" id="descrizione">{{__('ui.previousPage')}}                
                  </button>
                </a>
                  
        </div>
      </div>


  
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    <script>
      let swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });
    </script>   
</x-layout>