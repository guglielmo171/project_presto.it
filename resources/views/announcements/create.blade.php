<x-layout>
    <h2 class="text-center mt-5">{{__('ui.addAnnouncement')}}</h2>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12" id="formspace">
                <livewire:create-announcements />
            </div>
        </div>
    </div>
</x-layout>