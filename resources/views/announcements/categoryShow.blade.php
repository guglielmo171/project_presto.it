<x-layout>

    <div class="container">

        <div>
            @switch($category->name)
            @case('Abbigliamento')
                <h2>{{__('ui.Clothing')}}</h2>
            @break
            @case('Arredamento')
                <h2>{{__('ui.Furnitures')}}</h2>
            @break
            @case('Sport')
                <h2>{{__('ui.Sport')}}</h2>
            @break
            @case('Libri')
                <h2>{{__('ui.Libri')}}</h2>
            @break
            @case('Fumetti')
                <h2>
                    {{__('ui.Fumetti')}}
                    
                    </li>
            @break
            @case('Antiquariato')
                <h2>{{__('ui.Antiquariato')}}</li>
            @break
            @case('Giardinaggio')
                <h2>{{__('ui.Giardinaggio')}}</li>
            @break
            @case('Motori')
                <h2>{{__('ui.Motori')}}</h2>
            @break
            @case('Tecnologia')
                <h2>
                   {{__('ui.Tecnologia')}}
                </h2>
            @break
            @case('Cucina')
                <h2>
                    {{__('ui.Cucina')}}
                </h2>
            @break
          @endswitch
        </div>

        <div class="row justify-content-evenly">

            @forelse($category->announcements as $announcement)
                @if($announcement->is_accepted == true)
                {{-- @dd($announcements, $announcement) --}}
                <div class="col-12 col-lg-4">

                    <div class="card" style="place-items: center">

                        {{-- <img src="..." class="card-img-top" alt="..."> --}}
                        <div class="card-body">
                        <h5 class="card-title">{{__('ui.title')}}: {{$announcement->title}}</h5>
                        <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(397.6, 432) : "https://picsum.photos/200/300"}}" class="w-100 h-75" alt="image">
                        <p class="card-text">{{__('ui.catNav')}}: {{$announcement->category->name}}</p>
                        <p class="card-text">{{__('ui.price')}}: {{$announcement->price}}$</p>
                        <p class="card-text">{{__('ui.description')}}: {{$announcement->body}}</p>
                        <a href="{{route('index.show', compact('announcement'))}}" class="btn btn-primary">{{__('ui.details')}}</a>
                        </div>
                    </div>
                </div>
                @endif
            @empty
                <h3 class="text-center">{{__('ui.noAnnouncement')}}</h3>
                <a href="{{route('index.announcement')}}" class="btn btn-primary w-25">{{__('ui.allannouncements')}}</a>
                <a href="{{route('create.announcement')}}" class="btn btn-primary w-25">{{__('ui.create')}}</a>
            @endforelse
            
        </div>
    </div>  
</x-layout>