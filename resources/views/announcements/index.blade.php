<x-layout>

    <main>
        <div class="carta">
            @forelse ($announcements as $announcement)
                <div class="card" style="place-items: center">
                            <h5 class="card-title">{{__('ui.title')}}: {{$announcement->title}}</h5>
                            <img class="w-100  h-75" src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(397.6, 432) : "https://picsum.photos/200/300"}}"
                             alt="image">
                            <p class="card-text">{{__('ui.catNav')}}: {{$announcement->category->name}}</p>
                            <p class="card-text">{{__('ui.price')}}: {{$announcement->price}}$</p>
                            <p class="card-text">{{__('ui.description')}}: {{$announcement->body}}</p>
                            <a href="{{route('index.show', compact('announcement'))}}" class="btn btn-primary mt-2 w-100">{{__('ui.details')}}</a>
                </div>   
            @empty
                <div class="col-12">
                    <div class="alert alert-warning py-3 shadow">
                        <p class="leat">{{__('ui.noAnnouncement')}}</p>
                    </div>
                </div>
            @endforelse
            
        </div>
        <span class="container d-flex justify-content-center" id="paginate">
            {!! $announcements->links() !!}

        </span>

    </main>
</x-layout>