<div>
   
    <form id="createform" wire:submit.prevent="createAnnouncements">
        
        {{-- titolo annuncio --}}
        <div class="form-outline mb-4">
            <label for="title">{{__('ui.productName')}}</label>
            <input type="text" id="title" wire:model.lazy="title" class="form-control @error('title') is-invalid @enderror">
            @error('title')
            <span class="error text-danger">{{$message}}</span>
            @enderror
        </div>
        
        
        {{-- categoria --}}
        <div class="form-outline mb-4">
            <label for="category">{{__('ui.chooseCategory')}}</label>
            <select class="form-control @error('category_id') is-invalid @enderror"  wire:model.lazy="category_id" id="category_id">
                <option disabled>Scegli un categoria</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">                        
                        @switch($category->name)
                            @case('Abbigliamento')
                            <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Clothing')}}</a></li>
                            @break
                            @case('Arredamento')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Furnitures')}}</a></li>
                            @break
                            @case('Sport')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Sport')}}</a></li>
                            @break
                            @case('Libri')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Libri')}}</a></li>
                            @break
                            @case('Fumetti')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Fumetti')}}</a></li>
                            @break
                            @case('Antiquariato')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Antiquariato')}}</a></li>
                            @break
                            @case('Giardinaggio')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Giardinaggio')}}</a></li>
                            @break
                            @case('Motori')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Motori')}}</a></li>
                            @break
                            @case('Tecnologia')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Tecnologia')}}</a></li>
                            @break
                            @case('Cucina')
                                <li><a class="dropdown-item" href="{{route('categoryShow', compact('category'))}}">{{__('ui.Cucina')}}</a></li>
                            @break
                        @endswitch
                   </option>
                @endforeach
            </select>
            @error('category_id')
                <span class="error text-danger">{{$message}}</span>
            @enderror
        </div>
        {{-- prezzo --}}
        <div class="form-outline mb-4">
            <label for="price">{{__('ui.price')}}</label>
            <input type="text" wire:model.lazy="price" class="form-control @error('price') is-invalid @enderror">
            @error('price')
            <span class="error text-danger">{{$message}}</span>
            @enderror
        </div>
        
        {{-- descrizione --}}
        <div class="form-outline mb-4">
            <label for="body">{{__('ui.shortDescription')}}</label>
            <textarea class="form-control @error('body') is-invalid @enderror" wire:model.lazy="body" id="body" rows="4" cols="10"></textarea>
            @error('body')
            <span class="error text-danger">{{$message}}</span>
            @enderror
        </div>

        {{--Immagine--}}
        <div class="mb-3">
            <input id="formFile"wire:model="temporary_images" type="file" multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror" placeholder="img"/>
            @error('temporary_images.*')
                <span class="text-danger mt-2 mb-3">{{$message}}</span>
            @enderror
        </div>
        @if (!empty($images))
            <div class="row">
                <div class="col-12">
                    <p>{{__('ui.photoPreview')}}</p>
                    <div class="row border border-4 border-info-rounded shadow py-4">
                        @foreach ($images as $key => $image)
                            <div class="col my-3">
                                    <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                                <button class="btn btn-danger shadow d-block text-center mt-2 mx-auto" type="button" wire:click="removeImage({{$key}})">{{__('ui.delete')}}</button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        
        
        
        
        
        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">{{__('ui.send')}}</button>
    </form>
</div>